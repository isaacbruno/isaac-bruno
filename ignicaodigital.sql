-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 28-Ago-2014 às 04:38
-- Versão do servidor: 5.5.34
-- versão do PHP: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `ignicaodigital`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `crud`
--

CREATE TABLE IF NOT EXISTS `crud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `body` longtext,
  `author` varchar(100) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `crud`
--

INSERT INTO `crud` (`id`, `title`, `slug`, `description`, `body`, `author`, `insert_date`, `update_date`) VALUES
(1, 'new title', NULL, 'new description', 'new body', 'new author', '2014-08-27 16:21:11', '2014-08-27 19:21:11'),
(2, 'new title', NULL, 'new description', 'new body', 'new author', '2014-08-27 17:00:03', '2014-08-27 20:00:03'),
(3, 'new title 3', NULL, 'new description', 'new body', 'new author', '2014-08-27 17:00:48', '2014-08-27 20:00:48'),
(6, 'Another post', NULL, 'description another post', 'body another post', 'Isaac Bruno', '2014-08-27 21:26:40', '2014-08-28 00:26:40'),
(8, 'Another post', NULL, 'funfou', 'hjhjj', 'Isaac Bruno', '2014-08-27 21:32:00', '2014-08-28 00:32:00'),
(10, 'Another post', NULL, 'k,hfjkhjkh', 'jkhjkh', 'jkjhkjhk', '2014-08-27 23:06:04', '2014-08-28 02:06:04');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
