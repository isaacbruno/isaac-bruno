<?php
class System {
    private $_url;
    private $_explode;
    public $_controller;
    public $_action;
    public $_params;
    
    public function __construct() {
        $this->setUrl();
        $this->setExplode();
        $this->setController();
        $this->setAction();
        $this->setParams();
    }
    
    private function setUrl(){
        $_GET['url'] = (isset($_GET['url']) ? $_GET['url'] : "index/index_action");
        $this->_url = $_GET['url'];
    }
    
    private function setExplode(){
        $this->_explode = explode("/", $this->_url);
    }
    
    private function setController(){
        $this->_controller = $this->_explode[0];
    }
    
    public function getController(){
        return $this->_controller;
    }
    
    private function setAction(){
        $ac = (!isset($this->_explode[1]) || $this->_explode[1] == null || $this->_explode[1] == 'index' ? 'index_action' : $this->_explode[1]);
        $this->_action = $ac;
    }
    
    public function setParams(){
        unset($this->_explode[0], $this->_explode[1]);
        
        if(end($this->_explode) == null){
            array_pop($this->_explode);
        }
        
        $i = 0;
        if(!empty($this->_explode)){
            foreach ($this->_explode as $val){
                if($i % 2 == 0){
                    $indices[] = $val;
                } else {
                    $valores[] = $val;
                }
                $i++;
            }
        } else {
            $indices = array();
            $valores = array();
        }
        
        if(count($indices) == count($valores) && !empty($indices) && !empty($valores)){
            $this->_params = array_combine($indices, $valores);
        } else {
            $this->_params = array();
        }
    }
    
    public function getParam($nome = null){
        if($nome != null){
            return $this->_params[$nome];
        } else {
            return $this->_params;
        }
    }
    
    public function run(){
        $controller_path = CONTROLLERS . $this->_controller . 'Controller.php';
        if(!file_exists($controller_path)){
            die("Missing Controller");
        }
        require_once $controller_path;
        $app = new $this->_controller();
        
        if(!method_exists($app, $this->_action)){
            die("Missing Action");
        }
        
        $action = $this->_action;
        $app->$action();
    }
}
