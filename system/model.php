<?php
class Model extends Database {
    public $_table;
    
    public function save(Array $data){
        $campos = implode(", ", array_keys($data));
        $valores = "'".implode("', '", array_values($data))."'";
        return $this->conn->query("INSERT INTO `{$this->_table}` ({$campos}, insert_date) VALUES ({$valores}, NOW())");
    }
    
    public function find($where = null){
        $where = (($where != null) ? "WHERE {$where}" : "");
        $stm = $this->conn->query("SELECT * FROM `{$this->_table}` {$where}");
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function delete($where){
        return $this->conn->query("DELETE FROM `{$this->_table}` WHERE {$where}");
    }
    
    public function update(Array $data, $where){
        foreach($data as $chave => $val):
            $campos[] = "{$chave} = '{$val}'";
        endforeach;
        $campos = implode(", ", $campos);
        return $this->conn->query("UPDATE `{$this->_table}` SET {$campos} WHERE {$where}");
    }
}
